defmodule ExDoc.Formatter.MD do
  @moduledoc """
    ExDoc.Formatter.MD
  """

  alias ExDoc.{Markdown, GroupMatcher}

  def run(project_nodes, config) when is_map(config) do
    config = %{config | output: Path.expand(config.output)}
    build = Path.join(config.output, ".build")

    output_setup(build, config)
    module = generate_list(project_nodes, config)
    generate_main(module, config)
    module
  end

  defp generate_main(module, config) do
    file_output = "#{config.output}/home.md"
    File.write!(file_output, "")

    Enum.each(module, fn item ->
      module_name = String.split(item, ".md")
      module_link = "[#{module_name}](#{module_name}) \n \n"
      File.write!(file_output, module_link, [:append])
    end)
  end

  defp output_setup(build, config) do
    if File.exists?(build) do
      build
      |> File.read!()
      |> String.split("\n", trim: true)
      |> Enum.map(&Path.join(config.output, &1))
      |> Enum.each(&File.rm/1)

      File.rm(build)
    else
      File.rm_rf!(config.output)
      File.mkdir_p!(config.output)
    end
  end

  defp generate_list(nodes, config) do
    nodes
    |> Enum.map(
      &Task.async(fn ->
        generate_module_page(&1, config)
      end)
    )
    |> Enum.map(&Task.await(&1, :infinity))
  end

  defp generate_module_page(module_node, config) do
    filename = "#{module_node.id}.md"
    file_output = "#{config.output}/#{filename}"
    File.write!(file_output, generate_module_title(module_node))

    module_node.docs
    Enum.map( module_node.docs ,fn(node_doc) ->
        append_module_docs(node_doc, file_output)
      end)

    filename
  end

  defp generate_module_title(module) do
    payload = "## " <> module.id <> " \n"

    if module.doc != nil do
      payload <> module.doc <> "\n"
    end

    payload
  end

  defp append_module_docs(module, file_output) do
    function_title = "### #{module.signature} \n"
    File.write!(file_output, function_title, [:append])

    if module.doc != nil do
      File.write!(file_output, module.doc, [:append])
    end
  end
end
